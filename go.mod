module g850xfer

go 1.16

require (
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4 // indirect
	github.com/pborman/getopt/v2 v2.1.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
