/*
 * Copyright (c) 2024 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package main

import (
	"fmt"
	"log"
	"bufio"
	"io/ioutil"
	"os"
	"github.com/jacobsa/go-serial/serial"
	"github.com/pborman/getopt/v2"
)

func main() {
	var filename string
	var devName string

	getopt.Flag(&filename, 'u', "upload file content to PC-G850")
	getopt.Flag(&devName, 'd', "path to serial device").Mandatory()

	getopt.Parse()

	config := serial.OpenOptions{
		PortName: devName,
		BaudRate: 9600,
		DataBits: 8,
		StopBits: 1,
		MinimumReadSize: 0,
		InterCharacterTimeout: 5000,
		ParityMode: serial.PARITY_ODD,
		Rs485Enable: false,
		Rs485RtsHighDuringSend: false,
		Rs485RtsHighAfterSend: false,
		RTSCTSFlowControl: true,
	}

	port, err := serial.Open(config)
	if err != nil {
		log.Fatal(err)
	}

	defer port.Close()

	if filename != "" {
		fi, err := os.Stat(filename)
		if err != nil {
			log.Fatal(err)
		}

		if fi.Size() > 32768 {
			log.Fatal("File too big for PC-G850 memory")
		}

		content, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Uploading to PC-G850...")

		// add EOF
		content = append(content, '\x1a')

		port.Write(content)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%d bytes sent\n", len(content))
		os.Exit(0)
	}

	reader := bufio.NewReader(port)
	data, err := reader.ReadBytes('\x1a')

	if err != nil {
		log.Fatal(err)
	}

	// trim trailling 0x1a and 0x0a at the end of file and print
	// fmt.Println(string(data[:]))
	fmt.Println(string(data[:len(data)-2]))
}
