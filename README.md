# g850xfer

Receive or send data (files) from or to Sharp PC-G850V pocket computer. This works both with the text editor's SIO function and with the machine langage monitor (flow control required).

Circuit :
```
                                   10 K             PC-G850V (SIO mode)
                                --/\/\/\--          ----------
    FTDI adapter                |        |          o 1  -
    ---------------------       |        |          o 2  VCC
    |               GND o ______|________o_________ o 3  GND
----   ----------   CTS o ______o__________________ o 4  RTS
|USB   | FT232R |    TX o ________________          o 5  DTR
----   ----------    RX o _______________ \________ o 6  RXD
    |               RTS o ______________ \_________ o 7  TXD
    ---------------------               \           o 8  CD
                                         \_________ o 9  CTS
 Use FT_PROG or ftdi_eeprom to invert               o 10 DSR
 RX, TX, RTS, and CTS logic levels.                 o 11 CI
 (or add 74x14/74x04 hex inverter)                  ----------
```

The serial link characteristics are hard-coded:

- 9600 bps
- 8 bit ata
- 1 stop bit
- Odd parity
- hadware flow control (RTS/CTS)

Usage:

- Receive:

```
./g850xfer -d /dev/ttyUSB0 > file.ext
```

- Send:

```
./g850xfer -d /dev/ttyUSB0 -u file.ext
```

